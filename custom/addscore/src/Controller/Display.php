<?php
/**
 * @file
 * Contains \Drupal\my_custom\Controller\Display.
 */

namespace Drupal\addscore\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class Display.
 *
 * @package Drupal\my_custom\Controller
 */
class Display extends ControllerBase {

  /**
   * showdata.
   *
   * @return string
   *   Return Table format data.
   */
  public function showdata() {

// you can write your own query to fetch the data I have given my example.
      // Get the definitions
    /*$definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', 'competition');
  
    // Load a node for which you want to get the field values
    $my_node = \Drupal\node\Entity\Node::load($my_nid);
  
    // Iterate through the definitions
    foreach (array_keys($definitions) as $field_name) {
      // Get the values for your node
      // Use getValue() if you want to get an array instead of text.
      $values[$field_name] = $my_node->get($field_name)->value;
    
     $results = db_query("SELECT * FROM {node} WHERE type = 'competition'", array(':uid' => $user->uid))->fetchField();
   foreach($results as $nid) {
      $node = node_load($nid);
      // Do something with $node
    }
    }*/  
    $user = \Drupal::currentUser(); 
    $userid = $user->id();
    $arguments = array(
      ':uid' => $userid
    );
    $result = \Drupal::database()->select('addscore', 'n')
            ->fields('n', array('id', '	competitions', 'chestno', '	mark'))
            ->where('uid=:uid', $arguments)
            ->execute()->fetchAllAssoc('id');
// Create the row element.
    $rows = array();
    foreach ($result as $row => $content) {
      $rows[] = array(
        'data' => array($content->id, $content->competitions, $content->chestno, $content->mark));
    }
// Create the header.
    $header = array('id', 'competitions', 'chestno', 'mark');
    $output = array(
      '#theme' => 'table',    // Here you can write #type also instead of #theme.
      '#header' => $header,
      '#rows' => $rows
    );
    return $output;
  }

}

